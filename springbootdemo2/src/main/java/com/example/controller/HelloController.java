package com.example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Administrator on 2017/1/17.
 */
@RestController
public class HelloController {
    @RequestMapping("/hello")
    public String say(){
        return "ni hao !";
    }
}
